class Api::UsersController < ApplicationController
  def create
    user = User.new(user_params)

    if user.save
      render json: { user: user }
    else 
      render json: { error: {code: 402, messages: user.errors.full_messages.join(",")} }
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :username, :password, :password_confirmation)
  end
end

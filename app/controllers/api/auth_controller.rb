class Api::AuthController < ApplicationController
  def create
    if params[:auth] 
      user = User.find_by(username: params[:auth][:username_or_email]) || User.find_by(email: params[:auth][:username_or_email])
    else 
      user = nil
    end

    if user && user.authenticate(params[:auth][:password])
      render json: { "jwt": JWT.encode({ user_id: user.id }, nil, 'none') }
    else 
      render json: { "error": {code: 422, message: "Sorry, your credentials is invalid"} }
    end
  end
end

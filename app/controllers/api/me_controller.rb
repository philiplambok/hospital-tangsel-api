class Api::MeController < ApplicationController
  def index
    begin
      auth_header = request.authorization
      auth_header_arr = auth_header.split
      decoded = JWT.decode(auth_header_arr[1], nil, false)
      payload = decoded[0]
      render json:  { user: User.find_by(id: payload["user_id"]) }
    rescue
      render json: { error: {code: 404, message: "Sorry, user not found"} }
    end
  end
end

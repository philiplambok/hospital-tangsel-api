Rails.application.routes.draw do
  namespace :api do 
    resources :users, only: [:create]
    resources :auth, only: [:create]
    resources :me, only: [:index]
  end
end

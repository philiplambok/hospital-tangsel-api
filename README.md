## Teknologi yang digunakan 
- Ruby (Sebagai bahasa pemrograman)
- Rails (Sebagai framework pada bahasa Ruby)
- PostgreSQL (Sebagai database)
- RSpec (Sebagai framework untuk pengujian)
- Postman (Pengujian API)

## Fitur-fitur yang disediakan 
- Sistem menyediakan user untuk melakukan registrasi 
- Sistem mendukung fitur login
- Sistem dapat menyediakan layanan untuk mengecek user yang sedang login

## Enpoints yang disediakan 
### User registrasi 
- **URL**  
  /api/users
- **Method**  
  `POST` 
- **Data Params**    
  ```json
  {
    "user": {
      "email": "yourmail@gmail.com", 
      "username": "your_user_name@gmail.com", 
      "password": "your_secret_password", 
      "password_confirmation": "your_secret_password"
    }
  }
  ```
- **Success Response**  
  Ketika request berhasil diproses atau user berhasil disimpan ke database, maka sistem akan mengembalikan objek user itu sendiri. 
  ```json
  {
    "user": {
      "email": "yourmail@gmail.com", 
      "username": "your_user_name@gmail.com", 
      "password": "your_secret_password", 
      "password_confirmation": "your_secret_password"
    }
  }
  ```
- **Error Response**  
  Namun jika terjadi kesalahan tertentu, maka sistem akan mengembalikan pesan error (contohnya jika username tidak diisi atau kosong)
  ```ruby
  {
    "error": {
      "code": 422, 
      "message": "Username can't be blank"
    }
  }
  ``` 

### User login  
- **URL**  
  /api/auth
- **Method**  
  `POST`
- **Data Params**   
  ```json
  {
    "auth": {
      "username_or_email": "your_username_or_your_email", 
      "password": "your_secret_password"
    }
  }
  ```
- **Success Response**  
  Ketika kredensial user benar, maka sistem akan mengembalikan token. Token ini sebagai tanda identikasi untuk user yang akan digunakan user di setiap requestnya dengan mengikuti format standar dari JWT: `authorization: Bearer insert_your_token_here` 
  ```json
  {
    "jwt": "eyJhbGciOiJub25lIn0.eyJ1c2VyX2lkIjoxfQ."
  } 
  ```
- **Error Response**  
  Ketika kredensialnya salah maka sistem akan mengembalikan pesan error. 
  ```json
  {
    "error":{
      "code": 422, 
      "message": "Sorry, you're credentials is invalid"
    } 
  } 
  ```

### Mengecek User yang sedang login 
Terkadang sistem juga membutuhkan informasi tentang siapa yang sedang login sekarang. Sistem dapat melakukan ini dengan bantuan verifikasi token yang sebelumnya sudah dibuat. User yang sudah login akan diberikan token yang unik dimana token tersebut akan selalu ditempelkan pada header pada setiap request. 
- **URL**  
  /api/me
- **Method**  
  `GET`
- **Data Params**   
  Tidak ada parameter pada body, cukup pada header saja `authorization: Bearer your_token`. 
- **Sucess Response**  
  Akan mengembalikan user yang teridentikasi. 
  ```json 
  {
    "user": {
      "email": "yourmail@gmail.com", 
      "username": "your_user_name@gmail.com", 
      "password": "your_secret_password", 
      "password_confirmation": "your_secret_password"
    }
  }
  ```
- **Error Respose**  
  Akan menampikan pesan error `not_found`
  ```json 
  {
    "error": {
      "code": 404, 
      "message": "Sorry, user not found"
    }
  }
  ```

### Schema Database 
```ruby
create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end
```

### Deploy 
[https://hospital-tangsel-api.herokuapp.com](https://hospital-tangsel-api.herokuapp.com)

### Beberapa literatur untuk referensi
- [JWT Introduction](https://jwt.io/introduction/)
- [RESTful API Codes](https://restfulapi.net/http-status-codes/)
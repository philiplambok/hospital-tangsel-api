require 'rails_helper'

RSpec.describe User, type: :model do
  it "is valid with email, username, password and password_confirmation" do 
    user = build(:user)
    user.valid?
    expect(user.errors).to be_empty
  end

  it "is invalid without username" do 
    user = build(:user, username: nil)
    user.valid? 
    expect(user.errors[:username]).to include("can't be blank")
  end

  it "is invalid without email" do 
    user = build(:user, email: nil)
    user.valid? 
    expect(user.errors[:email]).to include("can't be blank")
  end

  it "is invalid without password" do 
    user = build(:user, password: nil)
    user.valid? 
    expect(user.errors[:password]).to include("can't be blank")
  end

  it "is invalid with duplicate username" do 
    kotori = create(:user, username: "kotori")
    kotori_clone = build(:user, username: "kotori")
    kotori_clone.valid?
    expect(kotori_clone.errors[:username]).to include("has already been taken")
  end

  it "is invalid with duplicate email" do 
    kotori = create(:user, email: "kotori@gmail.com")
    kotori_clone = build(:user, email: "kotori@gmail.com")
    kotori_clone.valid?
    expect(kotori_clone.errors[:email]).to include("has already been taken")
  end
end

require 'rails_helper'

RSpec.describe "User Registration API", type: :request do
  describe "User Registration" do
    let(:user) { build(:user) }
    
    context "valid attributes" do
      it "return's user object" do
        post "/api/users", params: { user: attributes_for(:user) } 

        expect(response.body).to include(user.id.to_s, user.username, user.email)
      end
    end

    context "invalid attributes" do 
      it "returns errors response" do 
        post "/api/users", params: { user: attributes_for(:user, username: nil) } 

        expect(response.body).to include("Username can't be blank")
      end
    end
  end
end

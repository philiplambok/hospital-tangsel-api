require 'rails_helper'

RSpec.describe "Show Current User API", type: :request do
  context "with token" do 
    let (:user) { create(:user) }

    it "return's user object" do
      get '/api/me', headers: { authorization: "Bearer #{JWT.encode({user_id: user.id},nil, 'none')}" }
      expect(response.body).to include(user.username, user.email)
    end
  end

  context "without token" do 
    it "return's null on user" do
      get '/api/me'
      expect(response.body).to include("user", "Sorry, user not found")
    end
  end

  context "with invalid token" do 
    it "return's null on user" do
      get '/api/me', headers: { authorization: "Bearer t0k3n" }
      expect(response.body).to include("user", "Sorry, user not found")
    end
  end
end

require 'rails_helper'

RSpec.describe "User Login API", type: :request do
  describe "user login with valid credentials" do
    let(:user) { create(:user) }

    context "with username" do 
      it "return's token" do 
        post "/api/auth", params: { auth: { username_or_email: user.username, password: user.password } }

        expect(response.body).to include("jwt", JWT.encode({user_id: user.id}, nil, 'none'))
      end
    end

    context "with email" do 
      it "return's token" do 
        post "/api/auth", params: { auth: { username_or_email: user.email, password: user.password } }

        expect(response.body).to include("jwt", JWT.encode({user_id: user.id}, nil, 'none'))
      end
    end
  end

  describe "user login with invalid credentials" do
    let (:user) { build(:user) }

    it "return's error message" do 
      post "/api/auth", params: { auth: { username_or_email: user.username, password: user.password } }

      expect(response.body).to include("error", "422", "Sorry, your credentials is invalid")
    end
  end
end
